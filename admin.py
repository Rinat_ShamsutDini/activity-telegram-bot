import logging
import asyncio
from aiogram import Bot, Dispatcher, executor, types
from aiogram.utils.exceptions import CantInitiateConversation

from config import TOKEN, GROUP_CHAT_ID

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
INACTIVITY_THRESHOLD = 1296000
TIMEOUT = 300
last_activity = {}
last_activity1 = {}
bot = Bot(token=TOKEN)
dp = Dispatcher(bot)

# Обработчик команды /start в личных сообщениях
@dp.message_handler(chat_type=types.ChatType.PRIVATE)
async def private_msgs(message: types.Message):
    # Отправляем ответное сообщение
    await message.answer(f"Привет! Я бот, разработанный @RinatShamsutDini. С удовольствием отвечу на ваши вопросы.")

# Обработчик события присоединения нового участника к группе
@dp.message_handler(content_types=types.ContentType.NEW_CHAT_MEMBERS)
async def new_chat_members_handler(message: types.Message):
    try:
        # Приветствуем нового участника
        user = message.new_chat_members[0]
        await message.answer(f"Добро пожаловать в группу, {user.first_name}! Расскажите о себе. Если не расскажете, то бот автоматически удалит вас через {TIMEOUT} секунд.")

        # Запоминаем время присоединения нового пользователя
        last_activity1[user.id] = asyncio.get_event_loop().time()

        # Ожидаем сообщение от нового пользователя
        await asyncio.sleep(TIMEOUT)  # Ждем TIMEOUT секунд

        # Проверяем, было ли отправлено сообщение новым пользователем
        if user.id in last_activity1:
            
            elapsed_time = asyncio.get_event_loop().time() - last_activity1[user.id]
            
            if user.id in last_activity:
                await message.answer("Спасибо за ваше сообщение! Продолжайте общаться в группе.")
            elif elapsed_time >= TIMEOUT:            
                await message.chat.kick(user.id)  # Удаляем нового пользователя из группы
                await bot.unban_chat_member(message.chat.id, user.id)  # Разбаниваем пользователя
                
    except CantInitiateConversation: 
        pass    
            
        
# Обработка новых сообщений
@dp.message_handler(content_types=types.ContentType.ANY)
async def handle_message(message: types.Message):
    user_id = message.from_user.id
    last_activity[user_id] = asyncio.get_event_loop().time()

# Функция для проверки активности участников
async def check_activity(): 
    while True:             
        for user_id, timestamp in list(last_activity.items()):
            current_time = asyncio.get_event_loop().time()
            if current_time - timestamp > INACTIVITY_THRESHOLD:                
                if user_id in last_activity:  # Проверка наличия пользователя в словаре
                    user = await bot.get_chat_member(chat_id=GROUP_CHAT_ID, user_id=user_id)
                    if user.status in [types.ChatMemberStatus.CREATOR, types.ChatMemberStatus.ADMINISTRATOR]:
                        continue  # Пропускаем проверку администраторов
                    bot_info = await bot.get_me()
                    #administrators = await bot.get_chat_administrators(GROUP_CHAT_ID)
                    #print(administrators)
                    #for admin in administrators:
                        #if admin.status == "creator":
                            #owner_id = admin.user.id                            
                    if user_id != bot_info.id:  # Проверка на идентификатор бота и владельца группы
                        await bot.send_message(user_id, 'Вы не проявляли активность в течении 15 дней в группе https://t.me/+7JtfPnEBOKU2MDVi Если ты не напишешь о своих достижениях в группе в течении одного часа, данный бот тебя автоматически кикнет!')               
                
                        await asyncio.sleep(3600)  # Ожидание 3600 секунд
                
                        if user_id in last_activity and current_time - last_activity[user_id] > INACTIVITY_THRESHOLD:                 
                            await bot.kick_chat_member(chat_id=GROUP_CHAT_ID, user_id=user_id)
                            await bot.unban_chat_member(chat_id=GROUP_CHAT_ID, user_id=user_id)
                            del last_activity[user_id]
                            
        await asyncio.sleep(300)  # Проверка активности каждые 300 секунд

                       
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.create_task(check_activity())            
    executor.start_polling(dp, skip_updates=True)
