import os

from dotenv import load_dotenv

load_dotenv('.env')
TOKEN = os.environ.get('TOKEN')
GROUP_CHAT_ID = os.environ.get('GROUP_CHAT_ID')
